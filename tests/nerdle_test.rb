
require 'minitest/autorun'
require 'nerdle'

class NerdleTest < Minitest::Test

  def test_six
    tokens =  Nerdle::TokenStatus::Tokens.new
    item = Nerdle::Generate.six(tokens)
    p item
  end

  def test_five
    tokens =  Nerdle::TokenStatus::Tokens.new
    item = Nerdle::Generate.five(tokens)
    p item
  end

  def test_four
    tokens =  Nerdle::TokenStatus::Tokens.new
    item = Nerdle::Generate.four(tokens)
    p item
  end

end


