require 'set'

module Nerdle
  # Generate answers for nerdle
  class Generate
    # for the case where the answer has 1 digit
    # @return [String] a string of a digits and operators
    def self.six(tokens)
      ret = []
      size = 0
      required = tokens.required

      size = 0
      catch :done do
        [[2, 4, 0], [1, 4, 1], [1, 3, 2]].each do |e1, e2, off|
          of = [1,1,1]
          of[off] += 1
          tokens.get_ops(e1).each do |o1|
            tokens.get_ops(e2).each do |o2|
              Generator.gen('', tokens.gen(0, of[0])) do |a|
                Generator.gen('', tokens.gen(e1+1, of[1], o1)) do |b|
                  Generator.gen('', tokens.gen(e2+1, of[2], o2)) do |c|
                    Generator.gen('', tokens.gen(7,1)) do |x|
                      t = "%s %s %s %s %s = %s" % [ a, o1, b, o2, c, x ]
                      okv = "%s %s %s %s %s" % [ a, o1, b, o2, c ]
                      if o1 == '/' or o1 == '*'
                        xx = a.to_r.method(o1.to_sym).call(b.to_r).method(o2.to_sym).call(c.to_r)
                      else
                        xx = b.to_r.method(o2.to_sym).call(c.to_r).method(o1.to_sym).call(a.to_r)
                      end

                      next unless xx.denominator == 1

                      ok = xx
#puts "Fix me here %s %s %s %s %s [%s %s]" % [ a, o1, b, o2, c, ok, x]
                      next unless ok.to_i == x.to_i
#puts "Fix me here %s %s %s %s %s [%s %s]" % [ a, o1, b, o2, c, ok, x]
                      s = (a.split(//) + b.split(//) + c.split(//) + x.split(//) + [ o1,  o2]).uniq
                      next unless (s & required).size == required.size
                      uniqs = (s - required).size
                      if a.to_i * b.to_i * c.to_i == 0
                        uniqs -= 1 if uniqs > 0
                      end
                      next if uniqs < size

                      if uniqs > size
                        ret = Array.new
                        size = uniqs
                      end
                      ret.push t
                      throw :done if size == 7
                    end
                  end
                end
              end
            end
          end
        end
      end

      if ret.size == 0
        size = 0
        required = tokens.required

        [3].each do |e1|
          tokens.get_ops(e1).each do |o1|
            Generator.gen('', tokens.gen(0, e1)) do |a|
              Generator.gen('', tokens.gen(e1 + 1, 5 - e1, '+')) do |b|
                Generator.gen('', tokens.gen(7,1)) do |x|
                  t = "%s %s %s = %s" % [ a, o1, b, x ]

                  okv = "%s %s %s" % [ a, o1, b ]
#puts okv
                  if o1 == '/'
                    next unless a.to_i % b.to_i == 0
                  end
                  ok = eval okv
                  next unless ok.to_s == x.to_s

                  s = (a.split(//) + b.split(//) + x.split(//) + [ o1 ]).uniq
                  next unless (s & required).size == required.size
                  next if s.size < size
                  if s.size > size
                    ret = Array.new
                  end
                  ret.push t
                  size = s.size
                end
              end
            end
          end
        end
      end
      if ret.empty?
        return nil
      else
        return ret[-1].gsub(/ /, '')
      end
    end

    # for the case where the answer has 2 digits
    # @return [String] a string of a digits and operators
    def self.five(tokens)
      required = tokens.required
      size = 0
      ret = Array.new
      o1 = tokens.get_ops(1)
      o2 = tokens.get_ops(2)
      o3 = tokens.get_ops(3)

      catch :done do
        o1.each do |o1|
          o3.each do |o2|
            Generator.gen('', tokens.gen(0,1)) do |a| 
              Generator.gen('', tokens.gen(2,1,o1)) do |b| 
                Generator.gen('', tokens.gen(4,1,o2)) do |c| 
                  Generator.gen('', tokens.gen(6,2)) do |x| 
                    t = "%s %s %s %s %s = %s" % [ a, o1, b, o2, c, x ]
                    okv = "%s %s %s %s %s" % [ a, o1, b, o2, c ]

                    ok = eval okv
                    next unless ok.to_s == x.to_s
if (o1 == '/')
puts "%s %s %s %s" % [ a, b, a.to_i % b.to_i, a.to_i % b.to_i  ]
  next unless a.to_i % b.to_i == 0
end
if (o2 == '/')
  y = b
  if (o1 == '*')
    y = (a.to_i * b.to_i).to_s
  end
#puts "%s %s %s %s" % [ a, b, c, x ]
#puts "%s %s %s" % [ y, c, y.to_i % c.to_i ]
  next unless y.to_i % c.to_i == 0
end

                    s = (a.split(//) + b.split(//) + c.split(//) + x.split(//)).uniq
                    s = (a + b + c + x + o1 + o2).split(//).uniq

                    next unless (s & required).size == required.size
                    next if s.size < size
                    if s.size > size
                      ret = Array.new
                    end
                    ret.push t
                    size = s.size
                    throw :done if size == 7
                  end
                end
              end
            end
          end
        end
      end
      size = 0

      if (ret.size == 0)
        catch :done do
          o2.each do |o1|
            Generator.gen('', tokens.gen(0,2)) do |a| 
              Generator.gen('', tokens.gen(3,2,o1)) do |b| 
                Generator.gen('', tokens.gen(6,2)) do |x| 
                  if o1 == '/'
                    next if (a.to_i % b.to_i) != 0
                  end
                  ok = eval "%s %s %s" % [ a, o1, b ]
                  next unless ok.to_s == x.to_s
                  s = (a.split(//) + b.split(//) + x.split(//) + [ o1 ]).uniq

                  next unless (s & required).size == required.size

                  next unless s.size >= size

                  if s.size > size
                    ret = Array.new
                  end
                  ret.push "%s %s %s = %s" % [ a, o1, b, x ]
                  size = s.size
                  throw :done if size == 7
                end
              end
            end
          end
        end
      end

      if (ret.size == 0)
        catch :done do
          o3.each do |o3|
            Generator.gen('', tokens.gen(0,3)) do |a| 
              Generator.gen('', tokens.gen(4,1,o3)) do |b| 
                Generator.gen('', tokens.gen(6,2)) do |x| 
                  if o3 == '/'
                    next if (a.to_i % b.to_i) != 0
                  end
                  ok = eval "%s %s %s" % [ a, o3, b ]

                  next unless ok.to_s == x.to_s
                  s = (a.split(//) + b.split(//) + x.split(//) + [ o3 ]).uniq

                  next unless (s & required).size == required.size
                  next unless s.size >= size

                  if s.size > size
                    ret = Array.new
                  end
                  ret.push "%s %s %s = %s" % [ a, o3, b, x ]
                  size = s.size
                  throw :done if size == 7
                end
              end
            end
          end
        end
      end

      ret[-1].gsub(/ /, '')
    end

    # for the case where the answer has 3 digits
    # @return [String] a string of a digits and operators
    def self.four(tokens)
      required = tokens.required
      o1 = tokens.get_ops(1)
      size = 0
      ret = nil

      catch :done do
        [ 1, 2 ].each do |off1|
          tokens.get_ops(off1).each do |o1|
            catch :div do
              Generator.gen('', tokens.gen(0,off1)) do |a| 
                Generator.gen('', tokens.gen(off1 + 1,3-off1,o1)) do |b| 
                  Generator.gen('', tokens.gen(5,3)) do |x| 
                    t = "%s %s %s = %s" % [ a, o1, b, x ]
                    if o1 == '/'
                      throw :div if a.size < b.size 
                    end
                    okv = "%s %s %s" % [ a, o1, b ]
                    ok = eval okv
                    next unless ok.to_s == x.to_s
                    s = (a.split(//) + b.split(//) + x.split(//) + [ o1 ]).uniq
                    next unless (s & required).size == required.size
                    next if s.size < size
                    if s.size > size
                      ret = Array.new
                    end
                    ret.push t
                    size = s.size
#puts "%s %s" % [ size, t ]
                    throw :done if size == 7
                  end
                end
              end
            end
          end
        end
      end
      ret[-1].gsub(/ /, '')
    end

    # get an possible answer
    # @return [String] a string of a digits and operators

    def self.get
      et = $tokens['=']
      ret = nil

      if et.c == 0
        if (rand(200) == 1)
          return '9*8-7=65'
        else
          return '9+8*7=65'
        end
      elsif et.s.include? '5'
        return five($tokens) 
      elsif et.s.include? '6'
        return six($tokens) 
      elsif et.s.include? '4'
        return four($tokens) 
      else
        raise "Fixme --"
        pp et.s
        if et.s.include? '5'
            pp Hash( 'x' => $tokens.get_operators )
        elsif et.s.include? '6'
  #        return '16/8+2=4' if et.c == 1
  #       return '3-10+8=1' if et.c == 2
          return six($tokens)
        elsif et.s.include? '4'
          if $tokens['9'].d[0] == 'x'
            return '99+1=100'
          else
            return '1+99=100'
          end
        else
          raise "fix me"
        end
      end
  #    return ret
    end
  end

  class RetryError < StandardError
  end

  class Generator
    def self.gen(string, arrays, type='', &block)
      arrays[0].each do |v|
         if arrays.size > 1
           gen(string + v.to_s, arrays[1..-1], &block)
         else
           yield string + v.to_s
         end
      end
      string
    end
  end

  class TokenStatus
    attr_reader :c
    attr_reader :d
    attr_reader :exclude
    def initialize(v)
      @v = v
      @c = 0
      @d = Array.new(8)
      @exclude = false
      @include = false
    end
    def required?
      return @include
    end
    def validate(type, location)
      if (@c == 0)
        if type == '-'
          raise RuntimeError.new("Excluded value %s was included" % [ @v ]) if @include
          @exclude = true
        elsif type == 'x' or type == '+'
          raise RuntimeError.new("Excluded value %s was included" % [ @v ]) if @exclude
          @include = true
        end
      end
      @c += 1
      @d[location] = type
    end
    def is_operator?
      false
    end

  class Tokens
    def initialize
      @tokens = Hash.new
      0.upto(9).each do |n|
        @tokens[n.to_s] = NumberStatus.new(n)
      end
      ["+", "-", "*", "/"].each do |o|
        @tokens[o] = OperatorStatus.new(o)
      end
      @tokens['='] = EqualStatus.new('=')
    end

    def get_ops(offset)
      raise "bad offset" if offset == 0
      raise "bad offset" if offset > 4
      ret = []

      @tokens.each do |o, t|
        if t.d[offset] == 'x'
          if t.is_operator?
            ret = [ o ]
          end
          return ret
        end
      end

      ["*", "+", "-", "/"].each do |o|
        next if @tokens[o].exclude
        next if @tokens[o].d[offset] == '-'
        next if @tokens[o].d[offset] == '+'

        if @tokens[o].d[offset - 1] == 'x' || @tokens[o].d[offset + 1] == 'x'
          ret = [ ]
          break
        end
        if @tokens[o].d[offset] == 'x'
          ret = [ o ]
          break
        end
        ret.push o
      end
      return ret
    end
    def get_operators(*offsets)
      ret = Array.new
      [ '*', '-', '+', '/'].each do |o|
        unless @tokens[o].exclude
          ret.push o
        end
      end
      ret
    end
    def [](key)
      return @tokens[key]
    end

    # @return [Array(Symbol)] a list of required tokens
    def required
      ret = Array.new
      ((0..9).to_a + ['-', '+', '*', '/']).each do |k|
        if @tokens[k.to_s].required?
          ret.push k.to_s
        end
      end
      return ret
    end

    def validate(string, response)
      s = string.split(//)
      r = response.split(//)
      s.zip(r).each_with_index do |p, i|
        d = @tokens[p[0]]
        if (p[1] == 'x')
          eq = @tokens['=']
          if p[1] == '='
puts "Equal"
          end
          if (i >= 4 and i <= 6)
#puts "%s: %s %s" % [i, p[0], p[1] ]
#pp eq.s.delete(i.to_s)
          end
        end

        d.validate(p[1], i)
      end
    end

    def gen(offset, size, flag='')
      lists = []
      offset.upto(offset + size - 1).each do |o|
        a = (0..9).to_set
        a.to_a.each do |k|
          if @tokens[k.to_s].exclude
            a.delete k
          end
          if @tokens[k.to_s].d[o] == 'x'
            a = [ k ].to_set
          elsif @tokens[k.to_s].d[o] == '+'
            a.delete k
          end
        end
        lists.push a.to_a
      end
      if size > 1 or flag == '/'
        lists[0] -= [ 0 ]
      end
      return lists
    end
  end


  class NumberStatus < TokenStatus
    attr_reader :exclude
    def required
      return @include
    end
  end

  class OperatorStatus < TokenStatus
    def is_operator?
      true
    end
  end

  class EqualStatus < TokenStatus
    attr_reader :s
    def initialize(v)
      @s = ['4','5','6'].to_set
      @c = 0
      @d = []
    end
    def try
      raise "unused"
      case @s.size
      when 3
        ret = '5'
      when 2
        ret = '6'
      when 1
        ret = @s.to_a[0]
      else
        raise "ERROR"
      end
    end
    def validate(type, location)
      raise RetryError.new("There must be an equals sign in every equation") if type == '-'
      if type == '+'
        @s.delete(location.to_s)
      end
      if type == 'x'
        @s = [ location.to_s ].to_set
      end
      @c += 1
    end
  end
  end
end

