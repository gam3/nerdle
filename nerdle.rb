#!/usr/bin/ruby
$LOAD_PATH.unshift(File.dirname(__FILE__) + '/lib')
require 'pp' 
require 'getoptlong'
require 'set'
require 'nerdle'

opts = GetoptLong.new(
  [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
  [ '--answer', '-A',  GetoptLong::REQUIRED_ARGUMENT ],
)

opts.each do |opt, arg|
  case opt
  when '--help'
  when '--debug'
  when '--list'
  when '--answer'
    $answer = arg.gsub(/ /, '')
    $answerarray = $answer.split(//)
    $answerset = $answerarray.to_set
  end
end

$tokens = Nerdle::TokenStatus::Tokens.new

while string = Nerdle::Generate.get()
  begin
    puts ' ' + string
    s = string.split(//)
    response = ''
    if $answer
      $answer.split(//).zip(s).each do |a, b|
         if a == b
           response += 'x'
         elsif  $answerset.include?(b)
           response += '+'
         else
           response += '-'
         end
      end
      puts ' ' + response
    else
      print ":"
      response = gets
      break if response.nil?
      response.chomp!
      break if response == 'q'
    end
    break if response == 'xxxxxxxx'
    r = response.split(//)
    raise Nerdle::RetryError.new("Bad size") if response.size != 8
    $tokens.validate(string, response)
  rescue Nerdle::RetryError => e
    puts "try again:"
    retry
  rescue RuntimeError => e
    raise e
  end
end

